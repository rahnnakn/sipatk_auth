$('select').material_select();

// Initialize collapse button
$('.button-collapse').sideNav();
// Initialize collapsible (uncomment the line below if you use the dropdown variation)
// $('.collapsible').collapsible();

$('button.create-inventory').click(function() {
	var c = confirm('Tambahkan barang ini?');
	return c;
});

$('button.update-inventory').click(function() {
	var c = confirm('Ubah barang ini?');
	return c;
});

$('a.delete-inventory').click(function() {
	var c = confirm('Hapus barang ini?');
	return c;
});

$('button.create-card').click(function() {
	var c = confirm('Masukkan transaksi ini?');
	return c;
});

$('button.create-request').click(function() {
	var c = confirm('Pastikan informasi yang dimasukkan benar. Kirim permintaan barang?');
	return c;
});

$('button.update-request').click(function() {
	var c = confirm('Ubah permintaan ini?');
	return c;
});

$('a.delete-request').click(function() {
	var c = confirm('Hapus permintaan ini?');
	return c;
});

$('a.approve').click(function() {
	var c = confirm('Terima permintaan ini?');
	return c;
});

$('a.process').click(function() {
	var c = confirm('Proses permintaan ini?');
	return c;
});

$('a.finish').click(function() {
	var c = confirm('Selesaikan permintaan ini?');
	return c;
});

$('#addInventory').on('click', function() {
	$('select').material_select('destroy');
	$('#induk-semang').append($('.semang').first().clone());
	$('select').material_select();
	Materialize.updateTextFields();
});

$("select[required]").css({display: "inline", height: 0, padding: 0, width: 0});
