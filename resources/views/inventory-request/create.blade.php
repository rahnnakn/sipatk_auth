@extends('layouts.admin')
@section('page-title', 'Buat Permintaan | ')
@section('title', 'Buat Permintaan ATK')
@section('root', '<li><a href="/">Dashboard</a></li>')
<!-- buat user ini diilangin, jadi di if in -->
@section('previous')
  <li><a href="{{ route('request-index') }}">Daftar Permintaan</a></li>
@endsection
@section('here', 'Buat Permintaan')
@section('new_request', $count_requests)
@section('admin-content')
    <div class="col s12 m8 l9">
      <!-- <div class="card"> -->
      	<div id="jqueryvalidation" class="section">
	        <div class="container">
	            <h3 class="header">Buat Permintaan ATK</h3>
	            <br>
	            <!-- <div class="row centered"> -->
					<!-- <div class="card col s12 m4 l4" style="margin-bottom:20px;">
						<h4>Daftar Barang & Stok</h4>
						@foreach ($inventories as $inventory)
							{{ $inventory->name }} ()
						@endforeach
					</div> -->
					<!-- <div class="col s12 m12 l12" style="margin-bottom:20px;" id="request-create"> -->
						<div id="card-alert" class="card blue darken-1">
							<div class="card-content white-text darken-1">
								<p>Area dengan (*) wajib diisi.</p>	
								@if (session()->has('flash_message'))
									<p class="single-alert">{{ session('flash_message') }}</p>
							    @endif
							</div>
			            </div>
			            <br>
						<form class="row formValidate" id="formValidate" novalidate="novalidate" method="post" action="{{ route('request-create') }}">
							{{ csrf_field() }}
							<!--
								todo:
								x | nama dan divisi dari session
								x | tambah cancel barang
							-->
							<div id="induk-semang">
								<div class="semang">
									<div class="input-field col s11 m5 l5 inventory">
										<select name="inventory_name[]" data-error=".errorInventory" required="required">
											<option value="" disabled selected>Pilih Barang*</option>
											@foreach ($inventories as $inventory)
												<option value="{{ $inventory->id }}">{{ $inventory->name }} ({{ $inventory->cards->first()->stock }})</option>
											@endforeach
					                    </select>
					                    <div class="errorInventory"></div>
									</div>
									<div class="col s1 m1 l1 center slash"><br>/</div>
									<div class="input-field col s6 m3 l3 quantity validate">
										<label for="quantity">Jumlah*</label>
										<input type="text" name="quantity[]" data-error=".errorKuantitas">
										<div class="errorKuantitas"></div>
									</div>
									<div class="input-field col s6 m3 l3 unit">
										<!-- <label>Pilih Satuan</label> -->
										<select name="unit[]" data-error=".errorUnit" required="required">
											<option value="" disabled selected>Pilih Satuan*</option>
											@foreach ($units as $unit)
												<option value="{{ $unit->unit }}">{{ $unit->unit }}</option>
											@endforeach
										</select>
										<div class="errorUnit"></div>
									</div>
								</div>
							</div>
							<div class="col s12 m12 l12">
								<span id="addInventory" class="right btn waves-effect waves-light blue">+ Tambah barang</span>
							</div>
							<div class="input-field col s12 m12 l12 validate">
								<label for="purpose">Untuk Keperluan*</label>
								<textarea name="purpose" data-error=".errorAlasan" class="materialize-textarea""></textarea>
								<div class="errorAlasan"></div>
							</div>
							<div class="input-field col s12 m6 l6 validate">
								<!-- <label>Yang Menyetujui</label> -->
								<select name="approver" data-error=".errorApprover" required="required">
									<option value="" disabled selected>Yang Menyetujui*</option>
									@foreach ($users as $user)
										<option value="{{ $user->username }}">{{ $user->name }}</option>
									@endforeach
			                    </select>
			                    <div class="errorApprover"></div>
							</div>
							<div class="input-field col s12 m12 l12">
								<button class="btn waves-effect waves-light indigo darken-4 create-request">Simpan</button>
							</div>
						</form>
					<!-- </div> -->
	            <!-- </div> -->
	        </div>
		</div>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('assets/js/jquery-validation.min.js') }}"></script>
<script>
	$("#formValidate").validate({
        rules: {
            'inventory_name[]': {
            	required: true,
            },
           	'quantity[]': {
                required: true,
                digits: true,
            },
            'unit[]': {
            	required: true,
            },
           	purpose: {
	            required: true,
            },
            'approver': {
            	required: true,
            }
		},
        //For custom messages
        messages: {
            'inventory_name[]': {
            	required: "Pilih barang yang akan dipesan",
            },
            'quantity[]': {
                required: "Isi jumlah barang yang akan dipesan",
                digits: "Jumlah barang harus berupa angka",
            }, 
            'unit[]': {
            	required: "Pilih satuan jumlah",
            },
            purpose:{
                required: "Isi alasan keperluan",
            },
            'approver': {
            	required: "Pilih pihak yang harus memberi persetujuan",
            }
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
			var placement = $(element).data('error');
			if (placement) {
			$(placement).append(error);
			} else {
			error.insertAfter(element);
			}
        }
     });
</script>
@endsection