@extends('layouts.admin')
@section('page-title', 'Permintaan | ')
@section('title', 'Permintaan')
@section('root', '<li><a href="/">Dashboard</a></li>')
@section('here', 'Daftar Permintaan')
@section('styles')
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}">
@endsection
@section('new_request', $count_requests)
@section('admin-content')
<div class="container col s12 m8 l9">
  <div class="right">
    <a href="{{ route('request-create') }}" class="btn waves-effect waves-light indigo darken-4"><i class="mdi-content-add"></i></a>
  </div>
  <div id="table-datatables">
    <h4 class="header">Daftar Permintaan</h4>
    <div class="row">
      <div class="col s12 m12 l12">
        @if (session()->has('flash_message'))
          <div id="card-alert" class="card blue darken-1">
            <div class="card-content white-text darken-1">
                <p class="single-alert">{{ session('flash_message') }}</p>
            </div>
          </div>
        @endif
        <table id="data-table-simple" class="responsive-table display centered" cellspacing="0">
          <thead>
              <tr>
                  <th>Pengaju</th>
                  <th>Status</th>
                  <th>Disetujui oleh</th>
                  <th>Penerima</th>
                  <th>Tanggal Dibuat</th>
                  <th></th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                  <th>Pengaju</th>
                  <th>Status</th>
                  <th>Disetujui oleh</th>
                  <th>Penerima</th>
                  <th>Tanggal Dibuat</th>
                  <th></th>
              </tr>
          </tfoot>
          <tbody>
            @foreach ($requests as $request)
            <tr>
              <td>{{ $request->u_sender->name }}</td>
              <td>
                <!-- 
                Status convention
                0 = Masuk
                1 = Disetujui
                2 = Diproses
                3 = Selesai 
                -->
                @if ($request->status == 1)
                  Disetujui
                @elseif ($request->status == 2)
                  Diproses
                @elseif ($request->status == 3)
                  Selesai
                @else
                  Masuk
                @endif
              </td>
              <td>
                @if ($request->approver != null && $request->status >= 1)
                  {{ $request->u_approver->name }}
                @else
                  -
                @endif
              </td>
              <td>{{ $request->u_receiver == null ? '-' : $request->u_receiver->name}}</td>
              <td>{{ $request->created_at }}</td>
              <td>
                <a href="{{ route('request-detail', $request->id) }}" class="btn waves-effect waves-light light-blue darken-4"><i class="mdi-action-list"></i></a>
                @if ($request->status == 1) 
                  <a href="{{ route('request-process', $request->id) }}" class="btn waves-effect waves-light indigo darken-4 process tooltipped" data-position="bottom" data-delay="10" data-tooltip="Proses"><i class="mdi-action-cached"></i></a>
                  <a href="{{ route('request-delete', $request->id) }}" class="btn waves-effect waves-light blue darken-2 delete-request"><i class="mdi-content-clear"></i></a> 
                @elseif ($request->status == 2)
                  <a href="{{ route('request-finish', $request->id) }}" class="btn waves-effect waves-light indigo darken-4 finish tooltipped" data-position="bottom" data-delay="10" data-tooltip="Selesai"><i class="mdi-action-done"></i></a>
                  <a href="{{ route('request-update', $request->id) }}" class="btn waves-effect waves-light blue"><i class="mdi-editor-border-color"></i></a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/data-tables-script.js') }}"></script>
@endsection