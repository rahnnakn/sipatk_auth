@extends('layouts.admin')
@section('page-title', 'Bon Permintaan | ')
@section('title', 'Bon Permintaan')
@section('root', '<li><a href="/">Dashboard</a></li>')
@section('previous')
  <li><a href="{{ route('request-index') }}">Daftar Permintaan</a></li>
@endsection
@section('here', 'Bon Permintaan')
@section('styles')
  <style>
    p, ul li {
      font-size: 20px;
    }
  </style>
@endsection
@section('new_request', $count_requests)
@section('admin-content')
  @foreach ($request as $a_request)
    <div class="col s12 m4 l4">
      <div class="right-align">
        @if ($a_request->status == 0) 
          <!-- dan sesion username = approver -->
          <a href="{{ route('request-approve', $a_request->id) }}" class="btn waves-effect waves-light indigo darken-4 approve tooltipped" data-position="bottom" data-delay="10" data-tooltip="Terima" style="margin-left:20px;"><i class="mdi-action-thumb-up"></i></a>
        @elseif ($a_request->status == 1) 
          <!-- dan session role == admin -->
          <a href="{{ route('request-process', $a_request->id) }}" class="btn waves-effect waves-light indigo darken-4 process tooltipped" data-position="bottom" data-delay="10" data-tooltip="Proses" style="margin-left:20px;"><i class="mdi-action-cached"></i></a>
        @elseif ($a_request->status == 2) 
          <!-- dan session role == admin -->
          <a href="{{ route('request-finish', $a_request->id) }}" class="btn waves-effect waves-light indigo darken-4 finish tooltipped" data-position="bottom" data-delay="10" data-tooltip="Selesai" style="margin-left:20px;"><i class="mdi-action-done"></i></a>
          <a href="{{ route('request-update', $a_request->id) }}" class="btn waves-effect waves-light blue" style="margin-left:20px;"><i class="mdi-editor-border-color"></i></a>
        @endif
      </div>
      <br>
    </div>
    <div class="col s12 m8 l9">
      <div class="card">
        <div class="container">
          <div class="container">
            <br>
              <h3 class="header">Bon Permintaan #{{ $a_request->id }}</h3>
              <div class="row centered">
                <div class="col s12 m12 l12" style="margin-bottom:20px;">
                  @if (session()->has('flash_message'))
                      <div id="card-alert" class="card cyan lighten-5">
                        <div class="card-content cyan-text darken-1">
                            <p class="single-alert">{{ session('flash_message') }}</p>
                        </div>
                      </div>
                  @endif
                  <div id="card-alert" class="card blue darken-5">
                    <div class="card-content white-text">
                        <p class="single-alert">
                          @if ($a_request->status == 1)
                            Permintaan ini telah disetujui oleh {{ $a_request->u_approver->name }}, menanti untuk diproses.
                          @elseif ($a_request->status == 2)
                            Permintaan ini telah disetujui oleh {{ $a_request->u_approver->name }} dan sedang diproses.
                          @elseif ($a_request->status == 3)
                            Permintaan ini telah selesai diproses.
                          @else
                            Sedang menanti persetujuan {{ $a_request->u_approver->name }}.
                          @endif
                        </p>
                    </div>
                  </div>
                  <div class="col s12 m6 l6">
                    <h5>Pengaju:</h5>
                    <p><span style="border-bottom:1px dashed grey;">{{ $a_request->u_sender->name }}</span></p>
                  </div>
                  <div class="col s12 m6 l6">
                    <h5>Divisi Pengaju:</h5>
                    <p><span style="border-bottom:1px dashed grey;">{{ $a_request->u_sender->division->name }}</span></p>
                  </div>
                  <div class="col s12 m6 l6">
                    <h5>Nama barang/Jumlah:</h5>
                    <p>
                      <ul>
                        @foreach ($inventories as $inventory)
                          <li style="list-style-type:circle; margin-left: 25px;"><span style="border-bottom:1px dashed grey;"><a href="{{ route('card-detail', $inventory->inventory_id) }}">{{ $inventory->inventory->name }}</a> / {{ $inventory->quantity }} {{ $inventory->unit }}</span></li>
                        @endforeach
                      </ul>
                    </p>
                  </div>
                  <div class="col s12 m6 l6">
                    <h5>Untuk keperluan:</h5>
                    <p><span style="border-bottom:1px dashed grey;">{{ $a_request->purpose }}</span></p>
                  </div>
                  <div class="col s12 m12 l12">
                    <h5>Melalui persetujuan:</h5>
                    <p><span style="border-bottom:1px dashed grey;">{{ $a_request->u_approver->name }}</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    @endforeach
@endsection