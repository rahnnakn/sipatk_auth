@extends('layouts.admin')
@section('page-title', 'Barang Baru | ')
@section('title', 'Barang Baru')
@section('root', '<li><a href="/">Dashboard</a></li>')
@section('previous')
  <li><a href="{{ route('inventory-index') }}">Daftar Barang</a></li>
@endsection
@section('here', 'Barang Baru')
@section('new_request', $count_requests)
@section('admin-content')
<div class="col s12 m8 l9">
	<!-- <div class="card"> -->
		<div id="jqueryvalidation" class="section">
			<div class="container">
				<h4>Tambah Barang Baru</h4>
				<br>
				<!-- <div class="col s12 m12 l12" style="margin-bottom:20px;" id="inventory-create"> -->
					<div id="card-alert" class="card blue darken-1">
						<div class="card-content white-text darken-1">
							<p>Area dengan (*) wajib diisi.</p>
							@if (session()->has('flash_message'))
								<p class="single-alert">{{ session('flash_message') }}</p>
						    @endif
						</div>
		            </div>
					<form id="formValidate" class="row formValidate" method="post" action="{{ route('inventory-create') }}" novalidate="novalidate">
						{{ csrf_field() }}
						<div class="input-field col s12 m12 l12 validate">
							<label for="name">Nama Barang*</label>
							<input type="text" name="name" data-error=".errorName">
							<div class="errorName"></div>
						</div>

						<div class="input-field col s12 m6 l6 validate">
							<label for="min_stock">Stok Minimal*</label>
							<input type="text" name="min_stock" data-error=".errorMinStock">
							<div class="errorMinStock"></div>
						</div>
						<div class="input-field col s12 m6 l6 validate">
							<select name="category" data-error=".errorCategory" required="required">
								<option value="" disabled selected>Pilih Kategori*</option>
								<option value="1">DLP</option>
								<option value="2">Eproc</option>
								<option value="3">Lainnya</option>
							</select>
							<div class="errorCategory"></div>
						</div>
						<div class="col s12">
							<button class="btn waves-effect waves-light indigo darken-4 create-inventory">Simpan</button>
						</div>
					</form>
				<!-- </div> -->
			</div>
		</div>
	<!-- </div> -->
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
    $("#formValidate").validate({
        rules: {
            name: {
                required: true,
            },
           	min_stock: {
                required: true,
                digits: true,
            },
            category: {
	            required: true,
            },
		},
        //For custom messages
        messages: {
            name:{
                required: "Nama barang harus diisi",
            },
            min_stock: {
            	required: "Stok minimal harus diisi",
            	digits: "Stok minimal harus berupa angka",
            },
            category: {
            	required: "Kategori harus dipilih",
            }
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
</script>
@endsection

<!-- 
<form class="formValidate" id="formValidate" method="get" action="" novalidate="novalidate">
 -->