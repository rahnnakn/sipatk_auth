@extends('layouts.admin')
@section('page-title', 'Daftar Barang | ')
@section('title', 'Daftar Barang')
@section('root', '<li><a href="/">Dashboard</a></li>')
@section('here', 'Daftar Barang')
@section('styles')
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('assets/js/plugins/data-tables/css/jquery.dataTables.min.css') }}">
@endsection
@section('new_request', $count_requests)
@section('admin-content')
<div class="container col s12 m8 l9">
  <div class="right">
    <a href="{{ route('inventory-create') }}" class="btn waves-effect waves-light indigo darken-4"><i class="mdi-content-add"></i></a>
  </div>
  <div id="table-datatables">
    <h4 class="header">Daftar Barang</h4>
    <div class="row">
      <div class="col s12 m12 l12">
        @if (session()->has('flash_message'))
            <div id="card-alert" class="card blue darken-1">
              <div class="card-content white-text darken-1">
                  <p class="single-alert">{{ session('flash_message') }}</p>
              </div>
            </div>
            <br>
        @endif
        <table id="data-table-simple" class="responsive-table display centered" cellspacing="0">
          <thead>
              <tr>
                  <th>Nama</th>
                  <th>Kategori</th>
                  <th>Status</th>
                  <th>Stok Minimum</th>
                  <th>Stok Sekarang</th>
                  <th></th>
              </tr>
          </thead>
          <tfoot>
              <tr>
                  <th>Nama</th>
                  <th>Kategori</th>
                  <th>Status</th>
                  <th>Stok Minimum</th>
                  <th>Stok Sekarang</th>
                  <th></th>
              </tr>
          </tfoot>
          <tbody>
            @foreach ($inventories as $inventory)
            <tr>
              <td>{{ $inventory->name }}</td>
              <td>
                @if ($inventory->category == 1)
                  DLP
                @elseif ($inventory->category == 2)
                  Eproc
                @else
                  Lainnya
                @endif
              </td>
              <td>
                <!--
                    status convention:
                    0 - Kurang
                    1 - Cukup
                -->
                @if ($inventory->status == 0)
                  <span class="red-text">Kurang</span>
                @else
                  <span class="green-text">Cukup</span>
                @endif
              </td>
              <td>{{ $inventory->min_stock }}</td>
              <td>{{ $inventory->cards->first()->stock }}</td>
              <td>
                <a href="{{ route('card-detail', $inventory->id) }}" class="btn waves-effect waves-light light-blue darken-4"><i class="mdi-editor-insert-drive-file"></i></a>
                <a href="{{ route('inventory-update', $inventory->id) }}" class="btn waves-effect waves-light blue"><i class="mdi-editor-border-color"></i></a> 
                <a href="{{ route('inventory-delete', $inventory->id) }}" class="btn waves-effect waves-light light-blue darken-2 delete-inventory"><i class="mdi-content-clear"></i></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/data-tables/data-tables-script.js') }}"></script>
@endsection