@extends('layouts.admin')
@section('page-title', 'Ubah Barang | ')
@section('title', 'Ubah Barang')
@section('root', '<li><a href="/">Dashboard</a></li>')
@section('previous')
  <li><a href="{{ route('inventory-index') }}">Daftar Barang</a></li>
@endsection
@section('here', 'Ubah Barang')
@section('new_request', $count_requests)
@section('admin-content')
<div class="col s12 m8 l9">
	<div class="card">
		<div id="jqueryvalidation" class="section">
			<div class="container" id="inventory-create">
				<h4>Ubah Barang</h4>
				<br>
				<div id="card-alert" class="card blue darken-1">
					<div class="card-content white-text darken-1">
						<p>Area dengan (*) wajib diisi.</p>	
						@if (session()->has('flash_message'))
							<p class="single-alert">{{ session('flash_message') }}</p>
					    @endif
					</div>
	            </div>
				<form id="formValidate" class="row formValidate" method="post" action="{{ route('inventory-update', $inventory->id) }}">
					{{ csrf_field() }}
					<div class="col s12 m12 l12 validate">
						<label for="name">Nama Barang*</label>
						<div class="input-field">
							<input type="text" name="name" value="{{ $inventory->name }}" data-error=".errorName">
							<div class="errorName"></div>
					</div>
					</div>
					<div class="col s12 m6 l6 validate">
						<label for="min_stock">Stok Minimal*</label>
						<div class="input-field">
							<input type="text" name="min_stock" value="{{ $inventory->min_stock }}" data-error=".errorMinStock">
							<div class="errorMinStock"></div>
						</div>
					</div>
					<div class="col s12 m6 l6 validate">
						<label for="category">Pilih Kategori*</label>
						<div class="input-field">
							<select name="category" data-error=".errorCategory" required="required">
								@if ($inventory->category == 1)
									<option value="{{ $inventory->category }}" selected>DLP</option>
									<option value="2">Eproc</option>
									<option value="3">Lainnya</option>
								@elseif ($inventory->category == 2)
									<option value="{{ $inventory->category }}" selected>Eproc</option>
									<option value="1">DLP</option>
									<option value="3">Lainnya</option>
								@else 
									<option value="{{ $inventory->category }}" selected>Lainnya</option>
									<option value="1">DLP</option>
									<option value="2">Eproc</option>
									Lainnya
								@endif
							</select>
							<div class="errorCategory"></div>
						</div>
					</div>
					<div class="col s12">
						<button class="btn waves-effect waves-light indigo darken-4 update-inventory">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
    $("#formValidate").validate({
        rules: {
            name: {
                required: true,
            },
           	min_stock: {
                required: true,
                digits: true,
            },
            category: {
	            required: true,
            },
		},
        //For custom messages
        messages: {
            name:{
                required: "Nama barang harus diisi",
            },
            min_stock:{
                required: "Nama barang harus diisi",
                digits: "Stok minimal harus berupa angka",
            },
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
</script>
@endsection

