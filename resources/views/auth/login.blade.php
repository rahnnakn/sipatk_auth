@extends('layouts.main')

@section('styles')
<link href="{{ asset('assets/css/page-center.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
@endsection

@section('content')
<div id="login-page jqueryvalidation" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <form id="formValidate" class="login-form formValidate" novalidate="novalidate" method="post" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="input-field col s12 center">
                    <img src="assets/images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login">
                    <p class="center login-form-text">Sistem Informasi <br> Pengelolaan ATK - DPSI</p>
                </div>
            </div>
            @if (session()->has('flash_message'))
                <div id="card-alert" class="card blue darken-1">
                    <div class="card-content white-text darken-1">
                        <p class="single-alert">{{ session('flash_message') }}</p>
                    </div>
                </div>
            @endif
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-social-person-outline prefix"></i>
                    <label for="username" class="center-align">Username</label>
                    <input id="username" type="text" name="username" data-error=".errorUsername">
                    <div class="errorUsername" style="margin-left:3em;"></div>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12 validate">
                    <i class="mdi-action-lock-outline prefix"></i>
                    <label for="password">Password</label>
                    <input id="password" type="password" name="password" data-error=".errorPassword">
                    <div class="errorPassword" style="margin-left:3em;"></div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 validate">
                    <button class="btn waves-effect waves-light col s12 indigo darken-4">Login</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
    $("#formValidate").validate({
        rules: {
            username: {
                required: true,
            },
            password: {
                required: true,
            },
        },
        //For custom messages
        messages: {
            username:{
                required: "Username harus diisi",
            },
            password: {
                required: "Password harus diisi",
            },
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
</script>
@endsection