<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitQuantity extends Model
{
	protected $table = 'unit_quantities';
    /* add and update data */
    protected $fillable = [
        'unit',
    ];
}
