<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $guard = 'users';
    protected $primaryKey = 'username';
    public $incrementing = false;
    public $timestamps = false;

    public function division()
    {
        return $this->belongsTo(Division::class);
    }

    public function requests()
    {
        return $this->hasMany(InventoryRequest::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     
    protected $fillable = [
        'username', 
        'email', 
        'password',
    ];
    */

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     
    protected $hidden = [
        'password', 'remember_token',
    ];
    */
}
