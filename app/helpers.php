<?php

use App\Inventory;
use App\InventoryCard;
use App\BinInventory;
use App\BinRequest;
use App\BinRequestedInventory;

function get_stock($card, $in, $out)
{
    // $card = InventoryCard::where('inventory_id', $inventory->id)->orderBy('transaction_date', 'desc')->first();
	$current_stock = $card->stock;
    $current_stock += $in;
    $current_stock -= $out;

    return $current_stock;

    // initiate overall stock with 0
    // $current_stock = 0;
    // then for each row of card
    // foreach ($cards as $card) {
        // start from the earliest transaction
        // count stock for each card with (in - out), might get minus
        // $stock_per_card = $card->in - $card->out; 
        // then associate it with the overall stock
        // $current_stock += $stock_per_card;
        // update this stock of card with overall stock
        // InventoryCard::where('created_at', $card->created_at)
                        // ->update(['stock' => $current_stock]);
    // }
}

function new_binInventory($inventory)
{
    $new_bin = new BinInventory;
    $new_bin->id = $inventory->id;
    $new_bin->name = $inventory->name;
    $new_bin->status = $inventory->status;
    $new_bin->min_stock = $inventory->min_stock;
    $new_bin->category = $inventory->category;
    $new_bin->created_at = $inventory->created_at;
    $new_bin->updated_at = $inventory->updated_at;
    $new_bin->save();
}

function new_binRequest($request)
{
    $new_bin = new BinRequest;
    $new_bin->id = $request->id;
    $new_bin->author = $request->author;
    $new_bin->status = $request->status;
    $new_bin->purpose = $request->purpose;
    $new_bin->approver = $request->approver;
    if ($request->receiver != null) {
        $new_bin->receiver = $request->receiver;
    }
    $new_bin->created_at = $request->created_at;
    $new_bin->updated_at = $request->updated_at;
    $new_bin->save();
}

function new_binRequestedInventory($request)
{
    $r_inventories = $request->inventories()->get();
    foreach ($r_inventories as $r_inventory) {
        $new_bin = new BinRequestedInventory;
        $new_bin->id = $r_inventory->id;
        $new_bin->request_id = $r_inventory->request_id;
        $new_bin->inventory_id = $r_inventory->inventory_id;
        $new_bin->quantity = $r_inventory->quantity;
        $new_bin->unit = $r_inventory->unit;
        $new_bin->save();
    }
}

function new_cards($r_inventories, $a_request)
{
    foreach ($r_inventories as $r_inventory) {
        $latest_card = InventoryCard::where('inventory_id', $r_inventory->id)->get()->last();
        $inventory = Inventory::find($r_inventory->inventory_id);
        $card = new InventoryCard;
        $card->inventory_id = $r_inventory->inventory_id;
        $card->transaction_date = $a_request->updated_at->format('Y-m-d');
        $card->bill_num = 'Permintaan';
        $card->from = 'SLA';
        $card->to = $a_request->u_sender->name;
        $card->in = 0;
        $card->out = $r_inventory->quantity;
        $card->stock = get_stock($latest_card, $card->in, $card->out);
        $card->isShown = 1;
        $card->save();
        set_inventoryStatus($card, $inventory);
    }
}

function set_defaultCard($from, $to, $in, $out)
{
    if ($from == "")
        return $from = "-";
    if ($to == "")
        return $to = "-";
    if ($in == "")
        return $in = 0;
    if ($out == "")
        return $out = 0;
}

function set_inventoryStatus($new_card, $inventory)
{
    if ($new_card->stock <= $inventory->min_stock) {
        if ($inventory->status == 1) {
            new_binInventory($inventory, $inventory->id);

            Inventory::where('id', $inventory->id)->update([
                'status' => 0,
            ]);
        }
    } else {
        if ($inventory->status == 0) {
            new_binInventory($inventory, $inventory->id);

            Inventory::where('id', $inventory->id)->update([
                'status' => 1,
            ]);
        }
    }
}

function validation_request($input)
{
    $validator = [];
    $i = 0;
    if (isset($input['purpose']) && isset($input['approver'])) {
        $validator[$i] = Validator::make($input, [
                'purpose' => 'required',
                'approver' => 'required|exists:users,username',
        ]);
        $i++;
    }
    if (isset($input['inventory_name'])) {
        $inventories_name = $input['inventory_name'];
        $validator[$i] = Validator::make($inventories_name, [
            'required|exists:inventories,id',
        ]);
        $i++;
    }
    if (isset($input['quantity'])) {
        $inventories_quantity = $input['quantity'];
        $validator[$i] = Validator::make($inventories_quantity, [
            'required|integer',
        ]);
        $i++;
    }
    if (isset($input['unit'])) {
        $inventories_unit = $input['unit'];
        $validator[$i] = Validator::make($inventories_unit, [
            'required|exists:unit_quantities,unit',
        ]);
    }
    // return $validator;
    $hasFailed = false;

    foreach ($validator as $a_validator) {
        $hasFailed = $a_validator->fails() || $hasFailed;
    }

    return $hasFailed;
}