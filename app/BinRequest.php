<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinRequest extends Model
{
	protected $table = 'bin_requests';
    public $incrementing = false;

    public function request()
    {
        return $this->belongsTo(Request::class);
    }
}
