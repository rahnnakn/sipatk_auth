<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinRequestedInventory extends Model
{
    protected $table = 'bin_requested_inventories';
    public $incrementing = false;

    public function requested_inventory()
    {
        return $this->belongsTo(RequestedInventory::class);
    }
}
