<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function loginForm()
    {
        if (Auth::check()) {
            return redirect()->route('index');
        } 

        return view('auth.login');
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('login')
                    ->withErrors($validator)
                    ->withInput();
        }

        $input = $request->all();
        $username = $input['username'];
        $password = $input['password'];
        // return $input;
        $adServer = 'corp.bi.go.id';
        $ldapConn = ldap_connect($adServer);
        $ldaprdn = 'bank_indonesia' . '\\' . $username;

        ldap_set_option($ldapConn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldapConn, LDAP_OPT_REFERRALS, 0);

        $bind = @ldap_bind($ldapConn, $ldaprdn, $password);
        // return var_dump($bind);

        if ($bind) {
           // return "cek1";
            $filter = "(sAMAccountName=$username)"; // type exactly like this to parse the username
            // echo var_dump($filter);
            $result = ldap_search($ldapConn, 'dc=corp,dc=bi,dc=go,dc=id', $filter);
            // echo var_dump($result);
            ldap_sort($ldapConn, $result, 'sn');
            $info = ldap_get_entries($ldapConn, $result);
            // echo var_dump($info);
            for ($i = 0; $i < $info['count']; $i++) {
                if($info['count'] > 1)
                    break;

                session([
                    'username' => $info[$i]['samaccountname'][0],
                    'nip' => $info[$i]['employeeid'][0],
                    'division' => $info[$i]['department'][0],
                    'name' => $info[$i]['givenname'][0] .' '. $info[$i]['sn'][0],
                ]);

                // return session()->all();
            }
            @ldap_close($ldapConn);
            return redirect()->route('index');
        } else {
            session()->flash('flash_message', 'Username/password Anda salah.');
            return redirect()->route('login');
        }
    }

    public function logout()
    {
        session()->forget('username');
        session()->forget('nip');
        session()->forget('division');
        session()->forget('name');

        session()->flush();
        return redirect()->route('login');
    }
}
