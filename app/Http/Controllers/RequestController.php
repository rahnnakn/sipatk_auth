<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Http\Requests;
use App\BinRequest;
use App\Division;
use App\Inventory;
use App\InventoryRequest;
use App\RequestedInventory;
use App\UnitQuantity;
use App\User;

/*
 * Status convention
 * 0 = Masuk
 * 1 = Disetujui
 * 2 = Diproses
 * 3 = Selesai
 */

class RequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $requests = InventoryRequest::where('isShown', 1)->get();
        $count_requests = InventoryRequest::where('isShown', 1)->where('status', 0)->get()->count();
        // return $requests;
        // kalo session user == true, panggil method user
        return view('inventory-request.index', compact('requests', 'count_requests'));
    }

    public function detail($id)
    {
        $request = InventoryRequest::with('u_sender', 'u_receiver')->where('id', '=', $id)->get();
        $inventories = RequestedInventory::where('request_id', '=', $id)->get();
        $count_requests = InventoryRequest::where('isShown', 1)->where('status', 0)->get()->count();
    	return view('inventory-request.detail', compact('request', 'inventories', 'count_requests'));
    	// return $request;
    	// return $inventories;
    }

    public function create()
    {
        $count_requests = InventoryRequest::where('isShown', 1)->where('status', 0)->get()->count();
        $inventories = Inventory::where('isShown', 1)
                        ->orderBy('name', 'asc')
                        ->with(['cards' => function($q) {
                            $q->orderBy('updated_at', 'desc');
                        }])
                        ->get();
        $divisions = Division::all();
        $users = User::all();
        $units = UnitQuantity::all();
    	return view('inventory-request.create', compact('inventories', 'divisions', 'users', 'units', 'count_requests'));
        // return $units;
    }

    public function store(Request $request)
    {
        $input = $request->all();
        // return $input;
        if (validation_request($input)) {
            // return "cek1";
            session()->flash('flash_message', 'Ada kesalahan pada input barang.');
            foreach ($validator as $a_validator) {
                return redirect()->route('request-create')
                        ->withErrors($a_validator)
                        ->withInput();
            }
        } else {
            // return "cek2";
            $inventories_name = $input['inventory_name'];
            $inventories_quantity = $input['quantity'];
            $inventories_unit = $input['unit'];

            $input['author'] = 'annisks';
            $input['division'] = 1;
            $new_request = new InventoryRequest;
            $new_request->author = $input['author'];
            $new_request->status = 0;
            $new_request->purpose = $input['purpose'];
            $new_request->approver = $input['approver'];
            $new_request->isShown = 1;
            $new_request->save();

            $new_new_request = InventoryRequest::get()->last();

            for ($i = 0; $i < count($inventories_name); $i++) {
                $new_new_request->inventories()->create([
                    'inventory_id' => $inventories_name[$i],
                    'quantity' => $inventories_quantity[$i],
                    'unit' => $inventories_unit[$i],
                ]);
            }

            session()->flash('flash_message', 'Permintaan berhasil disimpan.');
            // kalo user berarti redirect ke index doi sendiri
            return redirect()->route('request-index');
        }
    }

    public function update($id)
    {
        $a_request = InventoryRequest::find($id);
        $inventories = $a_request->inventories()->get();
        $count_requests = InventoryRequest::where('isShown', 1)->where('status', 0)->get()->count();
        $units = UnitQuantity::all();
        
        return view('inventory-request.update', compact('a_request', 'inventories', 'count_requests', 'units'));
    }

    public function storeUpdate(Request $request, $id)
    {
        $input = $request->all();
        // return $input;
        if (validation_request($input)) {
            session()->flash('flash_message', 'Ada kesalahan pada input barang.');
            return redirect('permintaan/'.$id.'/ubah')
                    ->withErrors($validator)
                    ->withInput();
        } else {
            // return "oke";
            // $inventory = Inventory::find($id);
            // new_binInventory($inventory);
            // $input = $request->all();
            $inventories_quantity = $input['quantity'];
            $inventories_unit = $input['unit'];

            $a_request = InventoryRequest::find($id);
            new_binRequestedInventory($a_request);

            $r_inventories = $a_request->inventories()->get();
            $ids = [];
            $ii = 0;
            foreach ($r_inventories as $r_inventory) {
                $ids[$ii] = $r_inventory->inventory_id;
                $ii++;
            }
            // return $ids;
            for ($i = 0; $i < count($r_inventories); $i++) {
                RequestedInventory::where('request_id', $id)->where('inventory_id', $ids[$i])
                    ->update([
                        'quantity' => $inventories_quantity[$i],
                        'unit' => $inventories_unit[$i],
                    ]);
            }

            // return $inventory;
            session()->flash('flash_message', 'Permintaan berhasil diubah.');
            return redirect()->route('request-detail', $id);
        }
    }

    public function approve($id)
    {
        $a_request = InventoryRequest::find($id);
        // return $id;
        if ($a_request->status == 1) {
            session()->flash('flash_message', 'Maaf, permintaan ini telah disetujui.');
            return redirect()->route('request-detail', $id);  
        } else if ($a_request->status == 2) {
            session()->flash('flash_message', 'Maaf, permintaan ini sedang diproses.');
            return redirect()->route('request-detail', $id);  
        } else if ($a_request->status == 3) {
            session()->flash('flash_message', 'Maaf, permintaan ini telah selesai diproses.');
            return redirect()->route('request-detail', $id);  
        } else {
            // return "ja";
            new_binRequest($a_request);

            InventoryRequest::where('id', $id)->update([
                'status' => 1,
            ]);

            session()->flash('flash_message', 'Permintaan ini telah disetujui.');
            return redirect()->route('request-detail', $id);
        }
    }

    public function process($id)
    {
        $a_request = InventoryRequest::find($id);

        if ($a_request->status == 0) {
            session()->flash('flash_message', 'Maaf, permintaan ini belum disetujui.');
            return redirect()->route('request-detail', $id);  
        } else if ($a_request->status == 2) {
            session()->flash('flash_message', 'Maaf, permintaan ini sedang diproses.');
            return redirect()->route('request-detail', $id);  
        } else if ($a_request->status == 3) {
            session()->flash('flash_message', 'Maaf, permintaan ini telah selesai diproses.');
            return redirect()->route('request-detail', $id);  
        } else {
            new_binRequest($a_request);

            InventoryRequest::where('id', $id)->update([
                'status' => 2,
                'receiver' => 'skv',
            ]);

            session()->flash('flash_message', 'Permintaan ini sedang diproses.');
            return redirect()->route('request-detail', $id);
        }
    }

    public function finish($id)
    {
        $a_request = InventoryRequest::find($id);
        if ($a_request->status == 0) {
            session()->flash('flash_message', 'Maaf, permintaan ini belum disetujui.');
            return redirect()->route('request-detail', $id);  
        } else if ($a_request->status == 1) {
            session()->flash('flash_message', 'Maaf, permintaan ini belum diproses.');
            return redirect()->route('request-detail', $id);  
        } else if ($a_request->status == 3) {
            session()->flash('flash_message', 'Maaf, permintaan ini telah selesai diproses.');
            return redirect()->route('request-detail', $id);  
        } else {
            new_binRequest($a_request);

            InventoryRequest::where('id', $id)->update([
                'status' => 3,
            ]);

            // $inventories = RequestedInventory::where('request_id', $id)->get();
            $r_inventories = $a_request->inventories()->get();
            // return $inventories;
            new_cards($r_inventories, $a_request);
            
            session()->flash('flash_message', 'Permintaan ini telah selesai diproses.');
            return redirect()->route('request-detail', $id);
        }
    }

    public function delete(Request $request, $id)
    {
        $a_request = InventoryRequest::find($id);
        // dd($request);
        new_binRequest($a_request);

        InventoryRequest::where('id', $id)->update([
                'isShown' => 0,
            ]);
        // return $new_bin;
        return redirect()->route('request-index');
    }

    public function user($user)
    {
         $requests = InventoryRequest::where('author', '=', $user)->get();
         $count_requests = InventoryRequest::where('isShown', 1)->where('status', 0)->get()->count();

        return view('inventory-request.index', compact('requests', 'count_requests'));
    }
}
