<?php

// Route::group(['middleware' => ['web']], function () {
	// Route::auth();

	Route::get('/', [
		'as' => 'index',
		'uses' => 'DashboardController@index',
	]);

	Route::get('ldap-test', function() {
	    return view('ldap-test');
	});

	Route::get('bantuan', function() {
	    return view('help');
	});

	Route::get('test', function() {
	    return view('test');
	});

	Route::group(['prefix' => 'login'], function() {
		Route::get('/', [
			'as' => 'login',
			'uses' => 'Auth\AuthController@loginForm'
		]);
		Route::post('/', 'Auth\AuthController@authenticate');
	});

	Route::get('logout', [
		'as' => 'logout',
		'uses' => 'Auth\AuthController@logout'
	]);

	Route::group(['prefix' => 'barang'], function() {
		Route::get('/', [
				'as' => 'inventory-index',
				'uses' => 'InventoryController@index',
			]);
		Route::get('buat', [
				'as' => 'inventory-create',
				'uses' => 'InventoryController@create',
			]);
		Route::post('buat', 'InventoryController@store');
		Route::get('{id}/hapus', [
				'as' => 'inventory-delete',
				'uses' => 'InventoryController@delete',
			]);
		Route::get('{id}/ubah', [
				'as' => 'inventory-update',
				'uses' => 'InventoryController@update',
			]);
		Route::post('{id}/ubah', 'InventoryController@storeUpdate');
	});

	Route::group(['prefix' => 'kartu'], function() {
		Route::get('/', function() {
			return redirect()->route('inventory-index');
		});
		Route::get('{id}/ekspor', [
				'as' => 'card-export',
				'uses' => 'InventoryCardController@export',
			]);
		Route::get('{id}/buat', [
				'as' => 'card-create',
				'uses' => 'InventoryCardController@create',
			]);
		Route::post('{id}/buat', 'InventoryCardController@store');
		Route::get('{id}', [
				'as' => 'card-detail',
				'uses' => 'InventoryCardController@card'
			]);
	});

	Route::group(['prefix' => 'permintaan'], function() {
		Route::get('/', [
				'as' => 'request-index',
				'uses' => 'RequestController@index',
			]);
		Route::get('pengaju/{user}', [
				'as' => 'request-index-user',
				'uses' => 'RequestController@user',
			]);
		Route::get('buat', [
				'as' => 'request-create',
				'uses' => 'RequestController@create',
			]);
		Route::post('buat', 'RequestController@store');
		Route::get('{id}/ubah', [
				'as' => 'request-update',
				'uses' => 'RequestController@update',
			]);
		Route::post('{id}/ubah', 'RequestController@storeUpdate');
		Route::get('{id}/hapus', [
				'as' => 'request-delete',
				'uses' => 'RequestController@delete',
			]);
		Route::get('{id}/terima', [
				'as' => 'request-approve',
				'uses' => 'RequestController@approve',
		]);
		Route::get('{id}/proses', [
				'as' => 'request-process',
				'uses' => 'RequestController@process',
		]);
		Route::get('{id}/selesai', [
				'as' => 'request-finish',
				'uses' => 'RequestController@finish',
		]);
		Route::get('{id}', [
				'as' => 'request-detail',
				'uses' => 'RequestController@detail',
			]);
	});
// });
