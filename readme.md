# Development Configuration

1. Rename file .env.example menjadi .env dan atur DB sesuai konfigurasi environment Anda.
2. Jalankan `php artisan key:generate`
3. Jalankan `php artisan migrate:refresh`
4. Jalankan `php artisan db:seed` jika sebelumnya belum pernah dilakukan.
5. Jalankan `composer install`
6. Jalankan `npm install`
7. Jalankan `gulp`
8. Jalankan `gulp watch` jika berencana melakukan perubahan terhadap css/script.
9. Jalankan `php artisan serve`
10. Hack away!

# Code Wiki
# Controller
Most logic are presented on each respective modul. InventoryController for inventory, and so on.
# Helper
If certain method is to be used by more than one controller, then putting the method in the helper file is a good way to clear the bottleneck. It can also be considered as an act of making sure your code is maintainable and therefore can be reused regardless which modul calling it. Helper methods so far: 
### Get Current Stock
### Create New Bin for Inventory
### Create New Bin for Request
### Create New Bin for Requested Inventory
### Create New Cards for Finished Request
### Create A Card for New Inventory
### Set Inventory Status on Update
### Do a Validation for A Request