<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('author');
            $table->integer('status');
            $table->text('purpose');
            $table->string('approver');
            $table->string('receiver')->nullable();
            $table->boolean('isShown');
            $table->timestamps();

            $table->foreign('author')
                    ->references('NIP')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
            $table->foreign('receiver')
                    ->references('NIP')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
            $table->foreign('approver')
                    ->references('NIP')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('requests');
    }
}
