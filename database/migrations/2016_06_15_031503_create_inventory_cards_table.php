<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_cards', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->integer('inventory_id')->unsigned();
            $table->date('transaction_date');
            $table->string('bill_num');
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->integer('in')->nullable();
            $table->integer('out')->nullable();
            $table->integer('stock');
            $table->boolean('isShown');
            $table->timestamps();

            $table->foreign('inventory_id')
                    ->references('id')
                    ->on('inventories')
                    ->onDelete('restrict');

            $table->index([
                'inventory_id', 
                'bill_num'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inventory_cards');
    }
}
