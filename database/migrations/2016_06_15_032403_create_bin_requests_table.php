<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bin_requests', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->integer('id')->unsigned();
            $table->string('author');
            $table->integer('status');
            $table->text('purpose');
            $table->string('approver');
            $table->string('receiver')->nullable();
            $table->timestamps();

            $table->foreign('id')
                    ->references('id')
                    ->on('requests')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');

            $table->index([
                'id', 
                'created_at'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bin_requests');
    }
}
