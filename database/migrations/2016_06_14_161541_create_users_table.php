<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->string('username');
            $table->string('password');
            $table->string('NIP')->unique();
            $table->string('email');
            $table->string('name');
            $table->integer('division_id')->unsigned();
            $table->boolean('isAdmin');

            $table->foreign('division_id')
                    ->references('id')
                    ->on('divisions')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
